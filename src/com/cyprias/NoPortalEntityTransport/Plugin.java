package com.cyprias.NoPortalEntityTransport;


import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import org.bukkit.Bukkit;
import org.bukkit.Server;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.event.Listener;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import org.mcstats.Metrics;

import com.cyprias.NoPortalEntityTransport.configuration.Config;
import com.cyprias.NoPortalEntityTransport.configuration.YML;
import com.cyprias.NoPortalEntityTransport.listeners.EntityListener;

public class Plugin extends JavaPlugin {
	// static PluginDescriptionFile description;
	private static Plugin instance = null;

	
	//public void onLoad() {}

	public static HashMap<String, String> aliases = new HashMap<String, String>();
	public static Server server ;
	
	
	
	public void onEnable() {
		instance = this;
		server = this.getServer();

		// Check if config.yml exists on disk, copy it over if not. This keeps our comments intact.
		if (!(new File(getDataFolder(), "config.yml").exists())) {
			Logger.info("Copying config.yml to disk.");
			try {
				YML.toFile(getResource("config.yml"), getDataFolder(), "config.yml");
			} catch (FileNotFoundException e) {
				e.printStackTrace();
				return;
			} catch (IOException e) {
				e.printStackTrace();
				return;
			}
		}
		
		//Check if the config on disk is missing any settings, tell console if so.
		try {
			Config.checkForMissingProperties();
		} catch (IOException e4) {
			e4.printStackTrace();
		} catch (InvalidConfigurationException e4) {
			e4.printStackTrace();
		}

		//Register our event listeners. 
		registerListeners(new EntityListener());
		
		if (Config.getBoolean("properties.use-metrics"))
			try {
				Metrics metrics = new Metrics(this);
				metrics.start();
			} catch (IOException e) {}
		

		String p = Plugin.getInstance().getServer().getClass().getPackage().getName();
		String version = p.substring(p.lastIndexOf('.') + 1);
		
		if (!version.equals("v1_6_R3") && !version.equals("v1_7_R1") && !version.equals("v1_7_R3")) {
			Logger.severe(instance.getName() + " won't work on Bukkit " + version);
			instance.getPluginLoader().disablePlugin(instance);
			return;
		}
		
    
		Logger.info("enabled.");
	}


	public void onDisable() {
		PluginManager pm = Bukkit.getPluginManager();
		server.getScheduler().cancelTasks(instance);
		instance = null;
		Logger.info("disabled.");
	}

	Listener[] listenerList;
	private void registerListeners(Listener... listeners) {
		PluginManager manager = getServer().getPluginManager();
		listenerList = listeners;
		for (Listener listener : listeners) {
			manager.registerEvents(listener, this);
		}
	}
	
	
	public static void reload() {
		instance.reloadConfig();
	}
	public static final Plugin getInstance() {
		return instance;
	}
}
