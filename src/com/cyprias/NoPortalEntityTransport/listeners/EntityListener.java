package com.cyprias.NoPortalEntityTransport.listeners;

import org.bukkit.Location;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityPortalEvent;

import com.cyprias.NoPortalEntityTransport.Logger;
import com.cyprias.NoPortalEntityTransport.Plugin;
import com.cyprias.NoPortalEntityTransport.configuration.Config;

public class EntityListener implements Listener {

	@EventHandler(priority = EventPriority.NORMAL)
	public void onEntityPortal(EntityPortalEvent event) {

		// Logger.info(event.getEntity());

		String p = Plugin.getInstance().getServer().getClass().getPackage().getName();
		String version = p.substring(p.lastIndexOf('.') + 1);

		Location loc = event.getEntity().getLocation();

		if (version.equals("v1_6_R3")) {
			if ((event.getEntity() instanceof org.bukkit.craftbukkit.v1_6_R3.entity.CraftItem) && (Config.getBoolean("properties.block-items"))) {
				Logger.debug("Blocking CraftItem from using portal at " + loc.getWorld().getName() + " " + loc.getBlockX() + " " + loc.getBlockY() + " " + loc.getBlockZ());
				event.setCancelled(true);
			} else if ((event.getEntity() instanceof org.bukkit.craftbukkit.v1_6_R3.entity.CraftAnimals) && (Config.getBoolean("properties.block-items"))) {
				Logger.debug("Blocking CraftItem from using portal at " + loc.getWorld().getName() + " " + loc.getBlockX() + " " + loc.getBlockY() + " " + loc.getBlockZ());
				event.setCancelled(true);
			}

		} else if (version.equals("v1_7_R1")) {
			if ((event.getEntity() instanceof org.bukkit.craftbukkit.v1_7_R1.entity.CraftItem) && (Config.getBoolean("properties.block-items"))) {
				Logger.debug("Blocking CraftItem from using portal at " + loc.getWorld().getName() + " " + loc.getBlockX() + " " + loc.getBlockY() + " " + loc.getBlockZ());
				event.setCancelled(true);
			} else if ((event.getEntity() instanceof org.bukkit.craftbukkit.v1_7_R1.entity.CraftAnimals) && (Config.getBoolean("properties.block-items"))) {
				Logger.debug("Blocking CraftItem from using portal at " + loc.getWorld().getName() + " " + loc.getBlockX() + " " + loc.getBlockY() + " " + loc.getBlockZ());
				event.setCancelled(true);
			}
		} else if (version.equals("v1_7_R3")) {
			if ((event.getEntity() instanceof org.bukkit.craftbukkit.v1_7_R3.entity.CraftItem) && (Config.getBoolean("properties.block-items"))) {
				Logger.debug("Blocking CraftItem from using portal at " + loc.getWorld().getName() + " " + loc.getBlockX() + " " + loc.getBlockY() + " " + loc.getBlockZ());
				event.setCancelled(true);
			} else if ((event.getEntity() instanceof org.bukkit.craftbukkit.v1_7_R3.entity.CraftAnimals) && (Config.getBoolean("properties.block-items"))) {
				Logger.debug("Blocking CraftItem from using portal at " + loc.getWorld().getName() + " " + loc.getBlockX() + " " + loc.getBlockY() + " " + loc.getBlockZ());
				event.setCancelled(true);
			}
		}
	}
}
